/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <adtf_file/adtf_file_reader.h>
#include <adtf_file/standard_factories.h>
#include <catch2/catch.hpp>
#include <filesystem_compat.h>

namespace
{

adtf_file::Objects objects;
const bool initializer = []
{
    adtf_file::add_standard_objects();
    adtf_file::getObjects().push_back(std::make_shared<adtf_file::ADTFDatFileReaderFactory>());
    adtf_file::loadPlugin(DECOMPRESSION_READER_PLUGIN);
    return true;
}();

}

TEST_CASE("Decompression Default Properties", "")
{
    const auto source_file = GENERATE(
        TEST_SOURCE_DIR "/test_file.adtfdat.zip",
        TEST_SOURCE_DIR "/test_file.adtfdat.tar.gz",
        TEST_SOURCE_DIR "/test_file.adtfdat.lz4"
    );

    const fs::path expected = TEST_SOURCE_DIR "/test_file.adtfdat";

    std::error_code error;
    fs::remove(expected, error);

    const auto reader = adtf_file::getReaderFactories().openReader(source_file);
    REQUIRE(fs::exists(expected));

    size_t item_count = 0;
    for (;;)
    {
        try
        {
            reader->getNextItem();
            ++item_count;
        }
        catch(const adtf_file::exceptions::EndOfFile&)
        {
            break;
        }
    }

    REQUIRE(item_count == 42);
}

TEST_CASE("Decompression Explicit Properties", "")
{
    std::error_code error;
    fs::remove_all(TEST_BUILD_DIR "/dir1", error);
    const fs::path expected = TEST_BUILD_DIR "/dir1/dir2/dir3/test_file.adtfdat";

    {
        const auto reader = adtf_file::getReaderFactories().openReader(TEST_SOURCE_DIR "/test_file.adtfdat.zip",
        {
            {"extraction_directory", adtf_file::PropertyValue(expected.parent_path().string())},
            {"extracted_filename", adtf_file::PropertyValue("test_file.adtfdat")},
            {"delete_extracted", adtf_file::PropertyValue(true)},
        });

        REQUIRE(fs::exists(expected));
        fs::create_directory(TEST_BUILD_DIR "/dir1/blocking");
    }

    REQUIRE(!fs::exists(expected));
    REQUIRE(fs::exists(TEST_BUILD_DIR "/dir1"));
    REQUIRE(!fs::exists(TEST_BUILD_DIR "/dir1/dir2"));
}

TEST_CASE("File that looks like a tar archive but is raw", "")
{
    adtf_file::getReaderFactories().openReader(TEST_SOURCE_DIR "/test_file_like_tar_archive.adtfdat.lz4",
    {
        {"extraction_directory", adtf_file::PropertyValue(TEST_BUILD_DIR)}
    });
}
