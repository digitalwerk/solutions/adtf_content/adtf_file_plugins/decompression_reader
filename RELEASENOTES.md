# Release Notes

## Version History

- **1.1.0**
  - Correctly detect raw archives
  - Switch to subtree based worker_reader configuration
  - Embed generated adtffileplugin documentation page
  - Use ADTF File Library 0.16.0
- **1.0.0**
  - Initial version of a reader to extend the ADTF DAT Tool and ADTFDAT File Player to read compressed files/archives
  - Use ADTF File Library 0.13.3 and enable convenient support for ADTF >= 3.18

---

## Known Issues / Restrictions

- none