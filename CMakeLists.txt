# Copyright 2024 Digitalwerk GmbH.
#
#     This Source Code Form is subject to the terms of the Mozilla
#     Public License, v. 2.0. If a copy of the MPL was not distributed
#     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# If it is not possible or desirable to put the notice in a particular file, then
# You may include the notice in a location (such as a LICENSE file in a
# relevant directory) where a recipient would be likely to look for such a notice.
#
# You may add additional accurate notices of copyright ownership.

cmake_minimum_required(VERSION 3.23)

set(LABEL_NAME "Decompression Reader" CACHE STRING "The name for the project")
set(BUILD_VERSION "1.99.99" CACHE STRING "The version used for the build")
set(GIT_COMMIT "local" CACHE STRING "SHA-1 commit hash.")

string(TOLOWER "${LABEL_NAME}" PRJ_NAME)
string(MAKE_C_IDENTIFIER "${PRJ_NAME}" PRJ_NAME)
string(TOUPPER "${PRJ_NAME}_DIR" ADTF_ENVIRONMENT_DIRECTORY_MACRO)

project(${PRJ_NAME} VERSION ${BUILD_VERSION})
add_definitions(
    -DVERSION_MAJOR=${PROJECT_VERSION_MAJOR}
    -DVERSION_MINOR=${PROJECT_VERSION_MINOR}
    -DVERSION_PATCH=${PROJECT_VERSION_PATCH}
)

add_compile_definitions(BUILD_VERSION="${BUILD_VERSION}")

find_package(adtf_file REQUIRED)
find_package(dw_cmake_utilities REQUIRED)
find_package(adtf_file_tools CONFIG REQUIRED)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

enable_testing()

set (CMAKE_CXX_STANDARD 17)
set (CMAKE_CXX_STANDARD_REQUIRED TRUE)

set(GENERATED_DOC_DIR "${CMAKE_CURRENT_BINARY_DIR}/generated")
add_subdirectory(doc)
add_subdirectory(src)

configure_file(${PRJ_NAME}.adtfenvironment ${CMAKE_CURRENT_BINARY_DIR}/${PRJ_NAME}.adtfenvironment)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${PRJ_NAME}.adtfenvironment DESTINATION .)
install(FILES LICENSE README.md RELEASENOTES.md DESTINATION .)
