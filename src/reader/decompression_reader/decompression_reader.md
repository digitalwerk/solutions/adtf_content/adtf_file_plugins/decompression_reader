## Usage

To use the supplied reader, specify the plugin with the `--plugin` option of the `adtf_dattool` i.e.:

```
adtf_dattool --plugin /path/to/reader/bin/decompression_reader.adtffileplugin --liststreams /path/to/compressed_file.adtfdat.zip
```