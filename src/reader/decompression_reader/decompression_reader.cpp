/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <adtf_file/reader.h>
#include <licme/licme.h>

#include <archive.h>
#include <archive_entry.h>

#include <filesystem_compat.h>
#include <iostream>
#include <unordered_set>
#include <algorithm>

namespace
{

adtf_file::ReaderFactories getNonExplicitFactories()
{
    const auto original_factories = adtf_file::getReaderFactories();
    adtf_file::ReaderFactories non_explicit_reader_factories;

    for (const auto& factory: original_factories.getFactories())
    {
        if (!std::dynamic_pointer_cast<adtf_file::ExplicitReader>(factory.second->makeReader()))
        {
            non_explicit_reader_factories.add(factory.second);
        }
    }

    return non_explicit_reader_factories;
}

#define CHECK_ARCHIVE_RESULT(__exp, __archive)\
{\
    auto __result = (__exp);\
    if (__result < ARCHIVE_OK)\
    {\
        throw std::runtime_error(archive_error_string(__archive));\
    }\
}

void copy_data(struct archive* pInputArchive, struct archive* pOutputArchive)
{
    for (;;)
    {
        const void *buffer;
        size_t bytes_read;
        __LA_INT64_T offset;

        int result_code = archive_read_data_block(pInputArchive, &buffer, &bytes_read, &offset);
        CHECK_ARCHIVE_RESULT(result_code, pInputArchive);
        if (result_code == ARCHIVE_EOF)
        {
            break;
        }
        CHECK_ARCHIVE_RESULT(archive_write_data_block(pOutputArchive, buffer, bytes_read, offset),
                             pOutputArchive);
    }
}

std::vector<fs::path> extractArchive(const fs::path& filename,
                                     const fs::path& destination_directory,
                                     const fs::path& expected_file,
                                     bool raw_archive,
                                     std::ostream& debug)
{
    std::vector<fs::path> created_elements;

    struct archive *source_archive = archive_read_new();
    CHECK_ARCHIVE_RESULT(archive_read_support_filter_all(source_archive), source_archive);

    if (raw_archive)
    {
        CHECK_ARCHIVE_RESULT(archive_read_support_format_raw(source_archive), source_archive);
    }
    else
    {
        CHECK_ARCHIVE_RESULT(archive_read_support_format_all(source_archive), source_archive);
    }

    CHECK_ARCHIVE_RESULT(archive_read_open_filename(source_archive, fs::path(filename).make_preferred().string().c_str(), 16384), source_archive);

    struct archive* destination_archive = archive_write_disk_new();
    int write_flags = ARCHIVE_EXTRACT_TIME |
                 ARCHIVE_EXTRACT_PERM |
                 ARCHIVE_EXTRACT_ACL |
                 ARCHIVE_EXTRACT_FFLAGS;
    CHECK_ARCHIVE_RESULT(archive_write_disk_set_options(destination_archive, write_flags), destination_archive);
    CHECK_ARCHIVE_RESULT(archive_write_disk_set_standard_lookup(destination_archive), destination_archive);

    for (;;)
    {
        struct archive_entry *entry;

        int result_code = archive_read_next_header(source_archive, &entry);
        CHECK_ARCHIVE_RESULT(result_code, source_archive);
        if (result_code == ARCHIVE_EOF)
        {
            break;
        }

        const auto destination_filename = [&]()
        {
            const auto format = archive_format(source_archive);
            if (format != ARCHIVE_FORMAT_RAW)
            {
                return destination_directory / archive_entry_pathname(entry);
            }
            else
            {
                return expected_file;
            }
        }();

        std::vector<fs::path> parents_to_delete;
        auto remaining = destination_filename;
        while (remaining.has_parent_path())
        {
            remaining = remaining.parent_path();
            if (fs::exists(remaining))
            {
                break;
            }

            parents_to_delete.push_back(remaining);
        }
        created_elements.insert(created_elements.end(), parents_to_delete.rbegin(), parents_to_delete.rend());

        archive_entry_copy_pathname(entry, fs::path(destination_filename).make_preferred().string().c_str());

        debug << "extracting file " << destination_filename << "\n";

        CHECK_ARCHIVE_RESULT(archive_write_header(destination_archive, entry), destination_archive);

        copy_data(source_archive, destination_archive);

        CHECK_ARCHIVE_RESULT(archive_write_finish_entry(destination_archive), destination_archive);
        created_elements.push_back(destination_filename);
    }

    CHECK_ARCHIVE_RESULT(archive_read_close(source_archive), source_archive);
    CHECK_ARCHIVE_RESULT(archive_read_free(source_archive), source_archive);
    CHECK_ARCHIVE_RESULT(archive_write_close(destination_archive), destination_archive);
    CHECK_ARCHIVE_RESULT(archive_write_free(destination_archive), destination_archive);

    return created_elements;
}

}

class DecompressionReader: public adtf_file::SeekableReader
{
public:
    DecompressionReader()
    {
        FILE_PLUGIN_META_INFORMATION(BUILD_VERSION, "Decompression Reader to read compressed files/archives.")

        setConfiguration(
            adtf_file::PropertyBuilder("worker_reader", "")
                .setDescription("The identifier of the reader implementation. "
                                "If empty, the first compatible reader is automatically searched for. "
                                "Place the configuration for the worker reader as a sub tree to this property.")
            +
            adtf_file::PropertyBuilder("extraction_directory", "")
                .setDescription("If specified extract the archive to the given directory otherwise the file is"
                                "extracted next to the source archive.")
            +
            adtf_file::PropertyBuilder("extracted_filename", "")
                .setDescription("If specified, this filename is appended to the extraction directory to open"
                                "the delegate reader with. Otherwise the last extention is stripped from the"
                                "source filename (i.e. `data.adtfdat.zip` -> `data.adtfdat`).")
            +
            adtf_file::PropertyBuilder<bool>("delete_extracted", false)
                .setDescription("By default the extracted files are not delete when the reader is closed/destroyed."
                                "Mind that this will only delete files/directories that where in the original archive.")
            +
            adtf_file::PropertyBuilder<bool>("debug", true)
                .setDescription("By default `false`. If `true` debug output will be printed to `std::cerr`.")
        );
    }

    ~DecompressionReader()
    {
        _reader.reset();

        for (auto element = _to_delete.rbegin();
             element != _to_delete.rend(); ++element)
        {
            std::error_code error;
            if (!fs::remove(*element, error))
            {
                std::cerr << "unable to remove " << element->string() << ": " << error.message() << "\n";
            }
        }
    }

    std::string getReaderIdentifier() const override
    {
        return "decompression";
    }

    void open(const std::string& url,
              std::shared_ptr<adtf_file::SampleFactory> sample_factory,
              std::shared_ptr<adtf_file::StreamTypeFactory> stream_type_factory) override
    {
        const auto& configuration = getConfiguration();

        static std::ostream null_stream(nullptr);
        auto& debug = [&]() -> std::ostream&
        {
            if (adtf_file::getPropertyValue<bool>(configuration, "debug", ""))
            {
                return std::cerr;
            }
            else
            {
                return null_stream;
            }
        }();

        fs::path source_archive(url);

        static const std::vector<std::tuple<std::string, bool>> supported_extensions
        {
            {".zip", false },
            {".tgz", false },
            {".tar.gz", false },
            {".gz", true },
            {".lz4", true },
            {".tar.bz2", false },
            {".bz2", true },
            {".zst", true },
        };

        const auto supported_extension = std::find_if(supported_extensions.begin(), supported_extensions.end(),
        [&](const auto& extension)
        {
            const auto filename = source_archive.string();
            return filename.find(std::get<0>(extension)) == filename.size() - std::get<0>(extension).size();
        });

        if (supported_extension == supported_extensions.end())
        {
            throw std::runtime_error("Unsupported file.");
        }

        const auto destination_directory = [&]()
        {
            const auto property_value = adtf_file::getPropertyValue<std::string>(configuration, "extraction_directory", "");
            if (!property_value.empty())
            {
                return fs::path(property_value);
            }
            else
            {
                return source_archive.parent_path();
            }
        }();

        auto extracted_file_name = [&]()
        {
            const auto property_value = adtf_file::getPropertyValue<std::string>(configuration, "extracted_filename", "");
            if (!property_value.empty())
            {
                return destination_directory / property_value;
            }
            else
            {
                const auto filename = source_archive.filename().string();
                const auto filename_without_archive_extension = filename.substr(0, filename.size() - std::get<0>(*supported_extension).size());
                return destination_directory / filename_without_archive_extension;
            }
        }();
        extracted_file_name.make_preferred();

        debug << "extracting archive " << source_archive << " to " << destination_directory << "\n";

        _to_delete = extractArchive(source_archive, destination_directory, extracted_file_name, std::get<1>(*supported_extension), debug);
        if (!adtf_file::getPropertyValue<bool>(configuration, "delete_extracted"))
        {
            _to_delete.clear();
        }

        auto worker_reader_id = adtf_file::getPropertyValue<std::string>(configuration, "worker_reader", "");
        if (worker_reader_id.empty())
        {
            // fallback: support old property if the new one is not set.
            worker_reader_id = adtf_file::getPropertyValue<std::string>(configuration, "worker_reader_id", "");
        }

        std::optional<std::string> reader_id_helper;
        if (!worker_reader_id.empty())
        {
            reader_id_helper = worker_reader_id;
        }

        auto reader_factories = getNonExplicitFactories();

        debug << "opening " << extracted_file_name  << " with delegate reader\n";


        _reader = reader_factories.openReader(extracted_file_name.string(),
                                                adtf_file::getSubConfiguration(configuration, "worker_reader"),
                                                sample_factory,
                                                stream_type_factory,
                                                reader_id_helper);

        debug << "opened " << extracted_file_name << " with " << _reader-> getReaderIdentifier() << "\n";
    }

    adtf_file::FileItem getNextItem() override
    {
        return _reader->getNextItem();
    }

    uint32_t getFileVersion() const override
    {
        return _reader->getFileVersion();
    }

    std::string getDescription() const override
    {
        return _reader->getDescription();
    }

    std::vector<adtf_file::Extension> getExtensions() const override
    {
        return _reader->getExtensions();
    }

    std::vector<adtf_file::Stream> getStreams() const override
    {
        return _reader->getStreams();
    }

    std::optional<uint64_t> getItemCount() const override
    {
        return _reader->getItemCount();
    }

    std::optional<double> getProgress() const override
    {
        return _reader->getProgress();
    }

    uint64_t getItemIndexForTimeStamp(std::chrono::nanoseconds time_stamp) override
    {
        return checkSeekable()->getItemIndexForTimeStamp(time_stamp);
    }

    uint64_t getItemIndexForStreamItemIndex(uint16_t stream_id, uint64_t stream_item_index) override
    {
        return checkSeekable()->getItemIndexForStreamItemIndex(stream_id, stream_item_index);
    }

    std::shared_ptr<const adtf_file::StreamType> getStreamTypeBefore(uint64_t item_index, uint16_t stream_id) override
    {
        return checkSeekable()->getStreamTypeBefore(item_index, stream_id);
    }

    void seekTo(uint64_t item_index) override
    {
        checkSeekable()->seekTo(item_index);
    }

private:
    std::shared_ptr<adtf_file::SeekableReader> checkSeekable()
    {
        const auto seekable_reader = std::dynamic_pointer_cast<adtf_file::SeekableReader>(_reader);
        if (!seekable_reader)
        {
            throw std::runtime_error("Delegate reader does not support seeking.");
        }

        return seekable_reader;
    }
    std::shared_ptr<adtf_file::Reader> _reader;
    std::vector<fs::path> _to_delete;
};


namespace
{
    adtf_file::PluginInitializer initializer([]
    {
        adtf_file::getObjects().push_back(std::make_shared<adtf_file::ReaderFactoryImplementation<DecompressionReader>>());
    });
}
