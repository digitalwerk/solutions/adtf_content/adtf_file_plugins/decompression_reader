# Copyright 2024 Digitalwerk GmbH.
#
#     This Source Code Form is subject to the terms of the Mozilla
#     Public License, v. 2.0. If a copy of the MPL was not distributed
#     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# If it is not possible or desirable to put the notice in a particular file, then
# You may include the notice in a location (such as a LICENSE file in a
# relevant directory) where a recipient would be likely to look for such a notice.
#
# You may add additional accurate notices of copyright ownership.

from conans import ConanFile, tools, __version__ as conan_version
from conans.model.version import Version
from pathlib import Path
import os, re

class Deploy(ConanFile):
    name = "decompression_reader"
    label = "Decompression Reader"
    description = "Enhance ADTF File Library to read compressed files/archives."
    url = "https://gitlab.com/digitalwerk/solutions/adtf_content/adtf_file_plugins/decompression_reader.git"
    homepage = "https://gitlab.com/digitalwerk/solutions/adtf_content/adtf_file_plugins/decompression_reader"
    license = "MPL-2.0"

    settings = "os", "arch", "compiler", "build_type"

    short_paths = True

    def package_id(self):
        del self.info.settings.build_type

    def configure(self):
        if self.settings.compiler == "Visual Studio":
            del self.settings.compiler.runtime

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        pass

    def package_info(self):
        for path in Path('.').rglob('**/bin/**'):
            if re.match(r'[A-Za-z0-9\/]*bin(\/debug)?$', str(path)):
                self.env_info.path.append(os.path.join(self.package_folder, str(path)))
        self.user_info.ADTF3_ENVIRONMENT_FILES = [os.path.join(self.package_folder, self.name + ".adtfenvironment")]
